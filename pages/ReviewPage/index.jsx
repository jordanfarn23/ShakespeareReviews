import React from 'react';
import axios from 'axios';
import slice from 'lodash/slice';
import orderBy from 'lodash/orderBy';
import castArray from 'lodash/castArray';
import merge from 'lodash/merge';

import ReviewTable from '../../components/ReviewTable/index.jsx';
import PaginationButton from '../../components/PaginationButton/index.jsx';
import SortSelect from '../../components/SortSelect/index.jsx';
import Header from '../../components/Header/index.jsx';
import StatsBlock from '../../components/StatsBlock/index.jsx';

class ReviewPage extends React.Component {
  constructor() {
    super();
    this.state = {
      reviews: [],
      isLoading: true,
      pagination: 10,
      sortProperty: 'rating',
      sortType: 'desc',
    };
    this.reviewRatingTotal = 0;
  }

  async componentWillMount() {
    axios.defaults.headers.common.Authorization = 'koOheljmQX';
    const response = await axios.get('http://shakespeare.podium.co/api/reviews');
    if (response.status === 200) {
      const reviews = await Promise.all(response.data.data.map(async (datum) => {
        this.reviewRatingTotal += datum.rating;
        const reviewResponse = await axios.get(`http://shakespeare.podium.co/api/reviews/${datum.id}`);
        if (reviewResponse.status === 200) {
          return reviewResponse.data.data;
        }
        return merge(datum, { body: '' });
      }));
      this.setState({ reviews, isLoading: false });
    }
    this.setState({ isLoading: false });
  }

  sortReviews(reviews) {
    const { sortProperty, sortType } = this.state;
    return orderBy(reviews, castArray(sortProperty), castArray(sortType));
  }

  paginateReviews(reviews) {
    const { pagination } = this.state;
    const [start, end] = [1, pagination];
    return slice(reviews, start, end);
  }

  showMoreReviews() {
    const { state } = this;
    const { reviews, pagination } = this.state;
    if (pagination < reviews.length) {
      state.pagination += pagination;
      this.setState(state);
    }
  }

  updateSortOptions(event) {
    const sortValue = event.target.selectedOptions[0].value;
    const [sortProperty, sortType] = sortValue.split(' ');
    const { state } = this;
    state.sortProperty = sortProperty;
    state.sortType = sortType;
    this.setState(state);
  }

  render() {
    const {
      reviews, isLoading, pagination, sortProperty, sortType,
    } = this.state;
    return (
      <div>
        <div className="container-fluid">
          <Header />
        </div>
        <div className="container">
          <StatsBlock
            isLoading={isLoading}
            averageRating={this.reviewRatingTotal / reviews.length}
            totalRatings={reviews.length}
          />
          <SortSelect
            isLoading={isLoading}
            onChange={e => this.updateSortOptions(e)}
            value={`${sortProperty} ${sortType}`}
          />
          <ReviewTable
            isLoading={isLoading}
            reviews={this.paginateReviews(this.sortReviews(reviews))}
          />
          <PaginationButton
            isLoading={isLoading}
            pagination={pagination}
            onClick={() => this.showMoreReviews()}
            max={reviews.length}
          />
        </div>
      </div>
    );
  }
}

export default ReviewPage;
