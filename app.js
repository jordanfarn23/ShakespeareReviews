import React from 'react';
import ReactDOM from 'react-dom';
import ReviewPage from './pages/ReviewPage/index.jsx';

ReactDOM.render(<ReviewPage />, document.getElementById('app'));
