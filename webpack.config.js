/* eslint-disable no-unused-vars */
const webpack = require('webpack');
/* eslint-enable no-unused-vars */
const path = require('path');

module.exports = {
  entry: path.resolve(__dirname, 'app'),
  mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
  output: {
    path: `${__dirname}/dist`,
    publicPath: '/',
    filename: 'bundle.js',
  },
  devServer: {
    contentBase: path.resolve(__dirname, 'public'),
  },
  module: {
    rules: [
      { test: /\.(js|jsx)$/, exclude: /node_modules/, loaders: ['babel-loader'] },
      { test: /(\.css)$/, loaders: ['style-loader', 'css-loader'] },
    ],
  },
};
