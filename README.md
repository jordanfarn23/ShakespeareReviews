# Shakespeare Reviews

the frontend piece to http://shakespeare.podium.co. This renders all of shakespeares reviews in an easy to view, sortable manner. Also included is one of shakespeare's greatest.. works.. sort of.

## Set Up

### Must have Node.js version 8.11.2 (https://nodejs.org/en/download/)
### Must have npm version 5.6.0 (https://www.npmjs.com/get-npm)

## To Run Review App

First run
`npm i`
`npm start`

navigate to `http://localhost:8080/`

you will see the app parsing Shakespeare reviews

## Run Tests
`npm test`

## Run Linter
`npm run lint`
