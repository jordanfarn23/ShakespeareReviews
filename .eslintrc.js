module.exports = {
    "extends": "airbnb",
    "rules": {
      "import/extensions": "off",
      "react/jsx-filename-extension": "off"
    },
    "globals": {
      "describe": true,
      "it": true,
      "before": true,
      "document": true
    }
};
