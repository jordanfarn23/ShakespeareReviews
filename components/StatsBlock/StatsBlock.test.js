import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { expect } from 'chai';

import StatsBlock from './index.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('StatsBlock, (Component)', () => {
  let wrapper;
  const props = {
    isLoading: false,
    totalRatings: 100,
    averageRating: 3.266213,
  };

  before(() => {
    wrapper = Enzyme.shallow(<StatsBlock {...props} />);
  });

  it('returns null is isLoading is true', () => {
    wrapper.setProps({ isLoading: true });
    expect(wrapper.find('div').length).to.equal(0);
    wrapper.setProps({ isLoading: false });
  });

  it('renders the block headers', () => {
    const headerText = [
      'Average Review',
      'Number of Reviews',
    ];
    expect(wrapper.find('h1').map(header => header.text())).to.eql(headerText);
  });

  it('renders total ratings', () => {
    expect(wrapper.find('h3').text()).to.eql('100');
  });

  it('renders average ratings through StarRatings', () => {
    expect(wrapper.find('StarRatings').prop('rating')).to.eql(3.27);
  });
});
