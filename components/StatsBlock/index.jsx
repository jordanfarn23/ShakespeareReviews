import React from 'react';
import PropTypes from 'prop-types';

import StarRatings from 'react-star-ratings';

const StatsBlock = (props) => {
  const { isLoading, totalRatings, averageRating } = props;

  if (isLoading) return null;

  const columnStyles = {
    height: '300px',
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
  };

  const headerStyles = {
    fontFamily: 'Amatic SC',
    fontSize: '68px',
  };

  const totalRatingsStyle = {
    fontFamily: 'Amatic SC',
    fontSize: '48px',
  };

  const roundedAverage = Math.round(averageRating * 100) / 100;

  return (
    <div className="row">
      <div className="col text-center" style={columnStyles}>
        <h1 style={headerStyles}>Average Review</h1>
        <br />
        <StarRatings
          rating={roundedAverage}
          starRatedColor="gold"
          starDimension="50px"
          starSpacing="5px"
          name="avg-rating"
        />
      </div>
      <div className="col text-center" style={columnStyles}>
        <h1 style={headerStyles}>Number of Reviews</h1>
        <br />
        <h3 style={totalRatingsStyle}>{totalRatings}</h3>
      </div>
    </div>
  );
};

StatsBlock.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  averageRating: PropTypes.number.isRequired,
  totalRatings: PropTypes.number.isRequired,
};

export default StatsBlock;
