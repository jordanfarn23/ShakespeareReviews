import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import StarRatings from 'react-star-ratings';

const ReviewTable = (props) => {
  const { reviews = [], isLoading = false } = props;

  if (isLoading) {
    return (
      <div className="col text-center">
        <h3>Loading Reviews...</h3>
        <br />
        <img
          style={{ width: '8rem' }}
          className="fas fa-spin fa-lg fa-w-16"
          src="https://ichef.bbci.co.uk/images/ic/640xn/p03rdn4j.jpg"
          alt="spinning-shakespeare"
        />
      </div>
    );
  }

  return (
    <div className="row">
      <div className="col">
        <table className="table table-hover">
          <thead className="thead-light">
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Date</th>
              <th scope="col">Review</th>
              <th scope="col">Rating</th>
            </tr>
          </thead>
          <tbody>
            {
              reviews.map(review => (
                <tr key={review.id}>
                  <td className="author" style={{ minWidth: '180px' }}>
                    {review.author}
                  </td>
                  <td className="date" style={{ minWidth: '180px' }}>
                    {moment(review.publish_date).format('MMMM Do YYYY')}
                  </td>
                  <td className="body">{review.body}</td>
                  <td className="rating" style={{ minWidth: '250px' }}>
                    <StarRatings
                      rating={review.rating}
                      starRatedColor="gold"
                      starDimension="30px"
                      starSpacing="5px"
                      name="rating"
                    />
                  </td>
                </tr>
              ))
            }
          </tbody>
        </table>
      </div>
    </div>
  );
};

ReviewTable.propTypes = {
  reviews: PropTypes.arrayOf(
    PropTypes.shape({
      rating: PropTypes.number,
      publish_date: PropTypes.string,
      body: PropTypes.string,
      author: PropTypes.string,
    }),
  ).isRequired,
  isLoading: PropTypes.bool.isRequired,
};

export default ReviewTable;
