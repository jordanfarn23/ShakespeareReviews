import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { expect } from 'chai';

import ReviewTable from './index.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('ReviewTable, (Component)', () => {
  let wrapper;
  const props = {
    reviews: [
      {
        author: 'test 1', publish_date: '2016-09-05T23:25:47.642350Z', body: 'test body 1', rating: 2.67,
      },
      {
        author: 'test 2', publish_date: '2016-09-05T23:25:47.642350Z', body: 'test body 2', rating: 3.67,
      },
      {
        author: 'test 3', publish_date: '2016-09-05T23:25:47.642350Z', body: 'test body 3', rating: 4.67,
      },
    ],
    isLoading: false,
  };

  before(() => {
    wrapper = Enzyme.shallow(<ReviewTable {...props} />);
  });

  it('renders a loading screen if isLoading is true', () => {
    wrapper.setProps({ isLoading: true });
    expect(wrapper.find('h3').text()).to.equal('Loading Reviews...');
    expect(wrapper.find('img').prop('src')).to.eql('https://ichef.bbci.co.uk/images/ic/640xn/p03rdn4j.jpg');
    wrapper.setProps({ isLoading: false });
  });

  it('renders review authors', () => {
    const authors = [
      'test 1',
      'test 2',
      'test 3',
    ];
    expect(wrapper.find('.author').map(td => td.text())).to.eql(authors);
  });

  it('renders review publish_dates', () => {
    const dates = [
      'September 5th 2016',
      'September 5th 2016',
      'September 5th 2016',
    ];
    expect(wrapper.find('.date').map(td => td.text())).to.eql(dates);
  });

  it('renders review body', () => {
    const bodies = [
      'test body 1',
      'test body 2',
      'test body 3',
    ];
    expect(wrapper.find('.body').map(td => td.text())).to.eql(bodies);
  });

  it('renders review rating via StarRatings', () => {
    const ratings = [
      2.67,
      3.67,
      4.67,
    ];
    expect(wrapper.find('StarRatings').map(star => star.prop('rating'))).to.eql(ratings);
  });
});
