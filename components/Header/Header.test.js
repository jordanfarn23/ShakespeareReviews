import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { expect } from 'chai';

import Header from './index.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('Header, (Component)', () => {
  let wrapper;
  before(() => {
    wrapper = Enzyme.mount(<Header />);
  });

  it('renders the header', () => {
    expect(wrapper.find('h1').length).to.eql(1);
  });

  it('renders the correct styles', () => {
    expect(wrapper.find('.row').prop('style')).to.eql({ backgroundColor: '#0058AF', color: 'white' });
    expect(wrapper.find('h1').prop('style')).to.eql({ fontFamily: 'Amatic SC', fontSize: '120px' });
  });
});
