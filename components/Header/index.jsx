import React from 'react';

const rowStyles = {
  backgroundColor: '#0058AF',
  color: 'white',
};

const headerStyles = {
  fontFamily: 'Amatic SC',
  fontSize: '120px',
};

const Header = () => (
  <div className="row" style={rowStyles}>
    <div className="col text-center">
      <h1
        style={headerStyles}
      >
        Reviews of Shakespeare
      </h1>
    </div>
  </div>
);

export default Header;
