import React from 'react';
import PropTypes from 'prop-types';

const SortSelect = (props) => {
  const { isLoading, onChange, value } = props;

  if (isLoading) {
    return null;
  }

  const headerStyles = {
    fontFamily: 'Amatic SC',
  };

  return (
    <div className="row">
      <div className="col justify-content-md-left inline-block">
        <h1 className="d-inline" style={headerStyles}>Reviews</h1>
        <a
          style={headerStyles}
          className="d-inline"
          href="https://www.youtube.com/embed/4VBsi0VxiLg?rel=0&amp;autoplay=1;fs=0;autohide=0;hd=0;"
          rel="noopener noreferrer"
          target="_blank"
        >
          {'  Click For Classic Shakespeare'}
        </a>
      </div>
      <div className="col" />
      <div className="col">
        <select className="custom-select" id="sort-options" onChange={onChange} value={value}>
          <option value="rating desc">Sort By Rating High to Low</option>
          <option value="rating asc">Sort By Rating Low to High</option>
          <option value="publish_date desc">Sort By Date Newest to Oldest</option>
          <option value="publish_date asc">Sort By Date Oldest to Newest</option>
        </select>
      </div>
    </div>
  );
};

SortSelect.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
};

export default SortSelect;
