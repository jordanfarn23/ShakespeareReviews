import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { expect } from 'chai';

import SortSelect from './index.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('SortSelect, (Component)', () => {
  let wrapper;
  let changed = false;
  const props = {
    onChange: () => { changed = true; },
    isLoading: false,
  };

  before(() => {
    wrapper = Enzyme.shallow(<SortSelect {...props} />);
  });

  it('returns null if isLoading is true', () => {
    wrapper.setProps({ isLoading: true });
    expect(wrapper.find('div').length).to.equal(0);
    wrapper.setProps({ isLoading: false });
  });

  it('renders a link to a youtube video', () => {
    const link = wrapper.find('a');
    expect(link.prop('href')).to.equal('https://www.youtube.com/embed/4VBsi0VxiLg?rel=0&autoplay=1;fs=0;autohide=0;hd=0;');
    expect(link.text()).to.include('Click For Classic Shakespeare');
  });

  it('renders a header', () => {
    expect(wrapper.find('h1').text()).to.include('Reviews');
  });

  it('renders the correct options', () => {
    const optionTexts = [
      'Sort By Rating High to Low',
      'Sort By Rating Low to High',
      'Sort By Date Newest to Oldest',
      'Sort By Date Oldest to Newest',
    ];
    expect(wrapper.find('option').map(o => o.text())).to.eql(optionTexts);
  });

  it('calls on change when a new option is selected', () => {
    wrapper.find('select').simulate('change');
    expect(changed).to.equal(true);
  });
});
