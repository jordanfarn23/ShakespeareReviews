import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { expect } from 'chai';

import PaginationButton from './index.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('PaginationButton, (Component)', () => {
  let wrapper;
  let clicked = false;
  const props = {
    isLoading: false,
    onClick: () => { clicked = true; },
    pagination: 10,
    max: 20,
  };

  before(() => {
    wrapper = Enzyme.shallow(<PaginationButton {...props} />);
  });

  it('renders null when isLoading is true', () => {
    wrapper.setProps({ isLoading: true });
    expect(wrapper.find('div').length).to.equal(0);
    wrapper.setProps({ isLoading: false });
  });

  it('renders null when pagination is >= max ', () => {
    wrapper.setProps({ max: 10, pagination: 10 });
    expect(wrapper.find('div').length).to.equal(0);
    wrapper.setProps({ max: 20, pagination: 10 });
  });

  it('calls the onclick handler when clicked', () => {
    expect(clicked).to.equal(false);
    wrapper.find('button').simulate('click');
    expect(clicked).to.equal(true);
  });
});
