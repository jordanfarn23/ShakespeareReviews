import React from 'react';
import PropTypes from 'prop-types';

const PaginationButton = (props) => {
  const {
    isLoading, onClick, pagination, max,
  } = props;

  if (isLoading || (pagination >= max)) {
    return null;
  }

  return (
    <div className="row justify-content-md-center">
      <button
        type="button"
        className="btn btn-primary btn-lg"
        onClick={onClick}
      >
      Show More Reviews
      </button>
    </div>
  );
};

PaginationButton.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  pagination: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
};

export default PaginationButton;
